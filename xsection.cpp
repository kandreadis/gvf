#include "xsection.hpp"

void Xsection::CheckH(double h) {
  double hright=0.0;
  double hleft=0.0;
  for (int i=0;i<nsta;i++) {
    if (x[i]<=xbank[0] && z[i]>hleft) hleft=z[i];
    if (x[i]>=xbank[1] && z[i]>hright) hright=z[i];
  }
  if (h>hleft) {
    x.insert(x.begin(),x[0]);
    z.insert(z.begin(),h);
  }
  if (h>hright) {
    x.insert(x.end(),x[nsta-1]);
    z.insert(z.end(),h);
  }
  nsta=x.size();
}

void Xsection::Rectangular(double newz) {
  for (int i=0;i<nsta;i++) {
    if (x[i]>xbank[0] && x[i]<xbank[1]) {
      z[i]=newz;
    }
  }
}

// void Xsection::Triangular(double newz) {
//   for (int i=0;i<nsta;i++) {
//     if (x[i]>xbank[0] && x[i]<xbank[1]) {
//       double theta=PI/2.0-atan((xbank[1]-xbank[0])/(2.0*newz));
//       z[i]=tan(theta)*(xbank[0]+0.5*(xbank[1]-xbank[0])-x[i])+newz;
//     }
//   }
// }

void Xsection::CorrectBanks() {
  int xleft=0;
  while (x[xleft]<xbank[0]) xleft++;
  int xright=xleft;
  while (x[xright]<xbank[1] && xright<nsta) xright++;
  int xi=xleft;
  while (z[xi]==z[xi-1] && xi>=0) xi--;
  if (xi<xleft) xbank[0]=x[xi];
  xi=xright;
  while (z[xi]==z[xi+1] && xi<nsta) xi++;
  if (xi>xright) xbank[1]=x[xi];
}

void Xsection::CalcAPT(double h, double& A, double& P, double& T, double& Kt, double& alpha) {
  // check for valid WSE value
   CheckH(h);
  // find wetted locations
  int zi=0;
  while (x[zi]<xbank[0]) zi++;
  while (zi<nsta && z[zi]>h)
    zi++;
  int iminWet=zi;
  while (zi<nsta && z[zi]<=h)
    zi++;
  int imaxWet=zi-1;
  // calculate area and perimeter for all points except left and right edge
  double dx, dz;
  vector<double> Ai(nsta,0.0);
  vector<double> Pi(nsta,0.0);
  for (int i=iminWet;i<imaxWet;i++) {
    dx=x[i+1]-x[i];
    Ai[i]=0.5*dx*(2.0*h-z[i+1]-z[i]);
    if (Ai[i]<0.0) Ai[i]=0.0;
    dz=z[i+1]-z[i];
    Pi[i]=sqrt(dx*dx+dz*dz);
  }
  double xsLeft, xsRight, AsLeft, AsRight, PsLeft, PsRight;
  if (z[iminWet]>z[iminWet-1])
    xsLeft=x[iminWet];
  else
    xsLeft=x[iminWet]+(h-z[iminWet])/(z[iminWet-1]-z[iminWet])*(x[iminWet-1]-x[iminWet]);
  AsLeft=0.5*(x[iminWet]-xsLeft)*(h-z[iminWet]);
  PsLeft=sqrt((x[iminWet]-xsLeft)*(x[iminWet]-xsLeft)+(h-z[iminWet])*(h-z[iminWet]));
  if (z[imaxWet]>z[imaxWet+1])
    xsRight=x[imaxWet];
  else
    xsRight=x[imaxWet]+(h-z[imaxWet])/(z[imaxWet+1]-z[imaxWet])*(x[imaxWet+1]-x[imaxWet]);
  AsRight=0.5*(xsRight-x[imaxWet])*(h-z[imaxWet]);
  PsRight=sqrt((x[imaxWet]-xsRight)*(x[imaxWet]-xsRight)+(h-z[imaxWet])*(h-z[imaxWet]));
  A=accumulate(Ai.begin(),Ai.end(),0.0)+AsLeft+AsRight;
  P=accumulate(Pi.begin(),Pi.end(),0.0)+PsLeft+PsRight;
  T=xsRight-xsLeft;
  // partition channel
  vector<double> xall, Pall, Aall;
  xall.push_back(0.5*(xsLeft+x[iminWet]));
  Pall.push_back(PsLeft);
  Aall.push_back(AsLeft);
  for (int i=iminWet;i<imaxWet;i++) {
    xall.push_back(0.5*(x[i+1]+x[i]));
    Pall.push_back(Pi[i]);
    Aall.push_back(Ai[i]);
  }
  xall.push_back(0.5*(x[imaxWet]+xsRight));
  Pall.push_back(PsRight);
  Aall.push_back(AsRight);
  vector<double> Ps(3,0.0), As(3,0.0);
  for (size_t i=0;i<Pall.size();i++) {
    if (xall[i]<xman[1]) {
      Ps[0]+=Pall[i];
      As[0]+=Aall[i];
    }
    if (xall[i]>=xman[1] && xall[i]<xman[2]) {
      Ps[1]+=Pall[i];
      As[1]+=Aall[i];
    }
    if (xall[i]>=xman[2]) {
      Ps[2]+=Pall[i];
      As[2]+=Aall[i];
    }
  }
  // calculate conveyance and energy correction coefficient
  double Ks;
  Kt=0.0;
  alpha=0.0;
  for (int i=0;i<3;i++) {
    if (As[i]>0.0) {
      Ks=1.0/nman[i]*As[i]*pow(As[i]/Ps[i],2.0/3.0);
      Kt+=Ks;
      alpha+=(pow(Ks,3.0)/pow(As[i],2.0));
    }
  }
  alpha/=(pow(Kt,3.0)/pow(A,2.0));
}

int Xsection::GetNumStations() {
  return nsta;
}

double Xsection::GetFd() {
  return fd;
}

void Xsection::ChannelXsection(ifstream& file, string& line) {
  boost::char_separator<char> sep(" ");
  vector<string> tokens;
  while (line.find("#Sta")!=0)
    getline(file,line);
  boost::algorithm::split(tokens, line, boost::is_any_of("="));
  boost::algorithm::trim(tokens[1]);
  nsta=boost::lexical_cast<int>(tokens[1]);
  getline(file,line);
  while (line.find("#Mann")!=0) {
    Splitter tok(line,sep);
    tokens.assign(tok.begin(),tok.end());
    for (size_t i=0;i<tokens.size();i+=2) {
      boost::algorithm::trim(tokens[i]);
      boost::algorithm::trim(tokens[i+1]);
      x.push_back(boost::lexical_cast<double>(tokens[i])*FT_TO_M);
      z.push_back(boost::lexical_cast<double>(tokens[i+1])*FT_TO_M);
    }
    getline(file,line);
  }
  getline(file,line);
  Splitter tok(line,sep);
  tokens.assign(tok.begin(),tok.end());
  for (size_t i=0;i<tokens.size();i+=3) {
    boost::algorithm::trim(tokens[i]);
    boost::algorithm::trim(tokens[i+1]);
    xman.push_back(boost::lexical_cast<double>(tokens[i])*FT_TO_M);
    nman.push_back(boost::lexical_cast<double>(tokens[i+1]));
  }
  while (line.find("Bank")!=0) 
    getline(file,line);
  boost::algorithm::split(tokens, line, boost::is_any_of("=,"));
  boost::algorithm::trim(tokens[1]);
  boost::algorithm::trim(tokens[2]);
  xbank.push_back(boost::lexical_cast<double>(tokens[1])*FT_TO_M);
  xbank.push_back(boost::lexical_cast<double>(tokens[2])*FT_TO_M);
  zmin=9999.0;
  for (size_t i=0;i<z.size();i++)
    if (x[i]>=xbank[0] && x[i]<=xbank[1] && z[i]<zmin)
      zmin=z[i];
  // zmin=*min_element(z.begin(),z.end());
}

void Xsection::BridgeXsection() {
  nsta=0;
}

void Xsection::DamXsection() {
  nsta=0;
}
  
Xsection::Xsection(ifstream& file, string& line) {
  vector<string> tokens;
  boost::algorithm::split(tokens, line, boost::is_any_of(","));
  boost::algorithm::trim(tokens[1]);
  fd=boost::lexical_cast<double>(tokens[1])*FT_TO_M;
  string flag = tokens[2];
  getline(file,line);
  getline(file,desc);
  if (flag.empty() && desc.find("Bridge")!=string::npos)
    BridgeXsection();
  else if (flag.empty() && desc.find("Dam")!=string::npos)
    DamXsection();
  else
    ChannelXsection(file,line);
}
