#include <iostream>
#include <boost/bind.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include "xsection.hpp"

const double G=9.81;
const int NDX=100; // solver longitudinal increments

class River {
public:
  double hbc;
  double Qbc;
  vector<Xsection> xs;
  vector<double> qfd;
  vector<double> q;
  vector<double> flow;
  River(string);
  void RemoveSections();
  void RemoveSectionByFD(double);
  void SetBoundary(double,double);
  void TestCriticalDepth(Xsection&,Xsection&,vector<double>&,vector<double>&,double,vector<double>&,vector<double>&,vector<double>&);
  double SetCriticalDepth(Xsection&,double);
  void ReadFlows(string);
  vector<double> Prasad(double,Xsection&,Xsection&,vector<double>&,double);
  vector<double> Solve();
  void Write(vector<double>&,string);
  vector<double> Thalweg();
  void SetThalweg(vector<double>&);
  vector<vector<double> > Banks();
  vector<double> BedSlope();
  vector<double> FlowDistance();
  vector<double> Roughness();
  vector<double> FlowArea(vector<double>&);
  vector<double> TopWidth(vector<double>&);
  vector<double> Flow();
  void Rectangular();
  void ConstantSlopeThalweg(double,double);
  void PerturbThalweg(double,double);
  void PerturbThalwegWithSlope(double,double);
  void PerturbRoughness(double,double);
  void PerturbBanks();
  void PerturbLateralInflows(double,double);
};

// BOOST_PYTHON_MODULE(river) {
//   class_<vector<double> >("double_vector")
//     .def(vector_indexing_suite<vector<double> >())
//     ;

//   class_<vector<vector<double> > >("double_vvector")
//     .def(vector_indexing_suite<vector<vector<double> > >())
//     ;

//   class_<River>("River",init<string>())
//     .def_readwrite("q",&River::q)
//     .def_readonly("xs",&River::xs)
//     .def("read_flows",&River::ReadFlows)
//     .def("set_boundary",&River::SetBoundary)
//     .def("remove_empty",&River::RemoveSections)
//     .def("solve",&River::Solve)
//     .def("thalweg",&River::Thalweg)
//     .def("set_thalweg",&River::SetThalweg)
//     .def("banks",&River::Banks)
//     .def("bed_slope",&River::BedSlope)
//     .def("flow",&River::Flow)
//     .def("flow_distance",&River::FlowDistance)
//     .def("roughness",&River::Roughness)
//     .def("top_width",&River::TopWidth)
//     .def("flow_area",&River::FlowArea)
//     .def("rectangular",&River::Rectangular)
//     .def("const_slope_thalweg",&River::ConstantSlopeThalweg)
//     .def("perturb_thalweg",&River::PerturbThalweg)
//     .def("perturb_thalweg_wslope",&River::PerturbThalwegWithSlope)
//     .def("perturb_roughness",&River::PerturbRoughness)
//     .def("perturb_banks",&River::PerturbBanks)
//     .def("perturb_lateral_inflows",&River::PerturbLateralInflows)
//     ;
// }
