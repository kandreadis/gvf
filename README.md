Simple solver for Gradually Varied Flow water surface profile modeling. 

At its current state, it reads a HEC-RAS geometry file to build cross-sections, and uses a second order 
corrector-predictor solver to calculate steady-state profiles.

Developed for testing the SWOT instantaneous discharge algorithm.
