 #include <fstream>
#include <string>
#include <vector>
#include <numeric>
#include <cmath>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>
// #include <boost/python.hpp>
// #include <boost/python/suite/indexing/vector_indexing_suite.hpp>

using namespace std;
// using namespace boost::python;

typedef boost::tokenizer<boost::char_separator<char> > Splitter;

const double FT_TO_M=0.3048;
const double PI=3.14159;

class Xsection {
public:
  double zmin;
  double fd;
  string desc;
  int nsta;
  vector<double> x, z, xman, nman;
  vector<double> xbank;
  Xsection(ifstream&, string&);
  Xsection() {};
  void ChannelXsection(ifstream&, string&);
  void BridgeXsection();
  void DamXsection();
  int GetNumStations();
  double GetFd();
  void CorrectBanks();
  void CalcAPT(double,double&,double&,double&,double&,double&);
  void Rectangular(double);
  // void Triangular(double);
private:
  void CheckH(double);
};

// BOOST_PYTHON_MODULE(xsection) {
//   class_<std::vector<double> >("double_vector")
//     .def(vector_indexing_suite<std::vector<double> >())
//     ;

//   class_<Xsection>("Xsection")
//     .def("CalcAPT",&Xsection::CalcAPT)
//     ;
// }
