#include "river.hpp"

River::River(string filename) {
  ifstream file(filename.c_str());
  string line;
  while (file) {
    getline(file,line);
    if (line.find("Type")==0) {
      xs.push_back(Xsection(file,line));
    }
  }
  file.close();
}

void River::SetBoundary(double Q, double h) {
  hbc=h;
  Qbc=Q;
}

void River::RemoveSections() {
  xs.erase(remove_if(xs.begin(), xs.end(), boost::bind(&Xsection::GetNumStations, _1) == 0), xs.end());
}

void River::RemoveSectionByFD(double fdr) {
  // xs.erase(remove_if(xs.begin(), xs.end(), boost::bind(&Xsection::GetFd, _1) == fdr), xs.end());
  vector<Xsection> x1;
  x1.insert(x1.begin(),xs[0]);
  for (size_t i=1;i<xs.size();i+=20)
    x1.insert(x1.end(),xs[i]);
  x1.insert(x1.end(),xs[xs.size()-1]);
  xs=x1;
}

void River::TestCriticalDepth(Xsection& xsup, Xsection& xsdn, vector<double>& s, vector<double>& hi, double Q, vector<double>& Yh, vector<double>& Yn, vector<double>& Yc) {
  double alpha;
  double L=xsup.fd-xsdn.fd;
  double S0=(xsup.zmin-xsdn.zmin)/L;
  int npoints=hi.size();
  double A, P, T, K;
  for (int k=0;k<npoints;k++) {
    double dzv=S0*s[k];
    Xsection xsv(xsdn);
    for_each(xsv.z.begin(),xsv.z.end(),boost::lambda::_1+=dzv);
    xsv.zmin+=dzv;
    xsv.CalcAPT(hi[k],A,P,T,K,alpha);
    Yc[k]=pow(Q/sqrt(G)/T,2.0/3.0);
    Yh[k]=A/T;
    double nbed=pow(A,5.0/3.0)*pow(P,-2.0/3.0)/K;
    Yn[k]=pow(Q*nbed/T/sqrt(fabs(S0)),3.0/5.0);
  }
}

double River::SetCriticalDepth(Xsection& xsv, double Q) {
  double Hmin=9999.0;
  double h, V, H, A, P, T, K, alpha;
  double hc=0.0;
  double zmax=*max_element(xsv.z.begin(),xsv.z.end());
  double dz=(zmax-xsv.zmin)/(double)NDX;
  for (int i=0;i<NDX;i++) {
    h=xsv.zmin+0.1+dz;
    xsv.CalcAPT(h,A,P,T,K,alpha);
    V=Q/A;
    H=h+alpha*pow(V,2.0)/2.0/G;
    if (Hmin>H) {
      Hmin=H;
      hc=h;
    }
  }
  return hc;
}

void River::ReadFlows(string filename) {
  ifstream file(filename.c_str());
  string line;
  getline(file,line);
  getline(file,line);
  while (file) {
    getline(file,line);
    boost::trim(line);
    if (!line.empty()) {
      vector<string> tokens;
      boost::algorithm::split(tokens,line,boost::is_any_of("\t "));
      tokens.erase(remove_if(tokens.begin(),tokens.end(),boost::bind(&string::empty, _1)),tokens.end());
      boost::algorithm::trim(tokens[1]);
      qfd.push_back(boost::lexical_cast<double>(tokens[1])*FT_TO_M);
      boost::algorithm::trim(tokens[2]);
      q.push_back(boost::lexical_cast<double>(tokens[2])*pow(FT_TO_M,3.0));
    }
  }
  file.close();
}


vector<double> River::Prasad(double h, Xsection& xsup, Xsection& xsdn, vector<double>& s, double Q) {
  const double tolerance=1e-03;
  const int maxiter=1000;
  const double pm=-1.0;
  double A, P, T, K, alpha, F2, Sf, f0, f, fnew;
  double L=xsup.fd-xsdn.fd;
  double S0=(xsup.zmin-xsdn.zmin)/L;
  vector<double> hsol(s.size(),0.0);
  hsol[0]=h;
  xsdn.CalcAPT(hsol[0],A,P,T,K,alpha);
  F2=pow(Q,2.0)*T/(pow(A,3.0)*G);
  Sf=pow(Q,2.0)/pow(K,2.0);
  f0=pm*((S0-Sf)/(1.0-alpha*F2)-S0);
  for (size_t k=1;k<s.size();k++) {
    double dzv=S0*s[k];
    Xsection xsv(xsdn);
    for_each(xsv.z.begin(),xsv.z.end(),boost::lambda::_1+=dzv);
    xsv.zmin+=dzv;
    f=9999.0;
    fnew=f0;
    int iter=0;
    double hv=hsol[k-1];
    while (fabs(fnew-f)>tolerance && iter<maxiter) {
      f=fnew;
      hv=hsol[k-1]+0.5*(f0+f)*(s[k]-s[k-1]);
      if (hv<hsol[k-1]) {
	hv=hsol[k-1]+hsol[k-1]-hv;
	// cerr << "Warning, super-critical flow (Fr>1)!" << endl;
      }
      xsv.CalcAPT(hv,A,P,T,K,alpha);
      F2=pow(Q,2.0)*T/(pow(A,3.0)*G);
      Sf=pow(Q,2.0)/pow(K,2.0);
      fnew=pm*((S0-Sf)/(1.0-alpha*F2)-S0);
      iter++;
    }
    hsol[k]=hv;
    f0=fnew;
  }
  return hsol;
}

vector<double> River::Solve() {
  vector<double> h(xs.size(),0.0);

  h[xs.size()-1]=hbc;
  flow.push_back(Qbc);
  vector<double> s(NDX,0.0);
  vector<double> Yh, Yn, Yc;
  bool hascrit;
  for (int i=xs.size()-2;i>=0;i--) {
    // calculate total length between adjacent xs, and set up evaluation points
    double L=xs[i].fd-xs[i+1].fd;
    double dx=L/(double)(NDX-1);
    double S0=(xs[i].zmin-xs[i+1].zmin)/L;
    for (int j=1;j<NDX;j++) s[j]=dx*(double)j;
    // set up boundary conditions and flow rate
    size_t j=0;
    while (j<qfd.size() && qfd[j]>=xs[i+1].fd) j++;
    double Q=q[j-1]+flow[flow.size()-1]; // add upstream flow to lateral inflow/outflow
    flow.insert(flow.begin(),Q);
    vector<double> hi=Prasad(h[i+1],xs[i],xs[i+1],s,Q);
    vector<double> Yh(hi.size(),0.0), Yn(hi.size(),0.0), Yc(hi.size(),0.0);
    TestCriticalDepth(xs[i],xs[i+1],s,hi,Q,Yh,Yn,Yc);
    for (size_t j=0;j<Yn.size();j++) 
      if (Yh[j]<Yc[j] && Yc[j]>Yn[j]) hascrit=true;
    if (hascrit) {
      double dzv=S0*L;
      Xsection xsv(xs[i+1]);
      for_each(xsv.z.begin(),xsv.z.end(),boost::lambda::_1+=dzv);
      xsv.zmin+=dzv;
      double hc=SetCriticalDepth(xsv,Q);
      hi[hi.size()-1]=hc;
    }
    h[i]=hi[hi.size()-1];
  }

  return h;
}

void River::Write(vector<double>& h, string filename) {
  ofstream fout(filename.c_str());
  for (size_t i=0;i<h.size();i++)
    fout << (xs[i].fd/1000.0) << "\t" << h[i] << "\t" << xs[i].zmin << endl;
  fout.close();
}

vector<double> River::Thalweg() {
  vector<double> z(xs.size(),0.0);
  for (size_t i=0;i<xs.size();i++) z[i]=xs[i].zmin;
  return z;
}

void River::Rectangular() {
  for (vector<Xsection>::iterator x=xs.begin();x!=xs.end();x++)
    x->Rectangular(x->zmin);
}

vector<double> River::BedSlope() {
  vector<double> S0(xs.size(),0.0);
  for (size_t i=1;i<xs.size();i++)
    S0[i]=(xs[i-1].zmin-xs[i].zmin)/(xs[i-1].fd-xs[i].fd);
  return S0;
}

vector<double> River::FlowDistance() {
  vector<double> fd(xs.size(),0.0);
  for (size_t i=0;i<xs.size();i++)
    fd[i]=xs[i].fd;
  return fd;
}

void River::PerturbThalweg(double m, double s) {
  boost::mt19937 rnd;
  boost::normal_distribution<double> gd(m,s);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > varNor(rnd,gd);
  vector<double> S0=BedSlope();
  xs[0].zmin+=varNor();
  for (size_t i=1;i<xs.size();i++)
    xs[i].zmin=xs[i-1].zmin-S0[i]*(xs[i-1].fd-xs[i].fd);
}

void River::PerturbThalwegWithSlope(double Zbc, double S0var) {
  boost::mt19937 rnd;
  vector<double> S0=BedSlope();
  xs[xs.size()-1].zmin=Zbc;
  for (int i=xs.size()-2;i>=0;i--) {
    boost::normal_distribution<double> gd(1.0,S0var);
    boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > varNor(rnd,gd);
    double varS0=S0[i+1]*varNor();
    xs[i].zmin=xs[i+1].zmin+varS0*(xs[i].fd-xs[i+1].fd);
  }
}

void River::ConstantSlopeThalweg(double Zbc, double slope) {
  xs[xs.size()-1].zmin=Zbc;
  for (int i=xs.size()-2;i>=0;i--)
    xs[i].zmin=xs[i+1].zmin+slope*(xs[i].fd-xs[i+1].fd);
}

void River::PerturbRoughness(double m, double s) {
  boost::mt19937 rnd;
  boost::normal_distribution<double> gd(m,s);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > varNor(rnd,gd);
  for (vector<Xsection>::iterator x=xs.begin();x!=xs.end();x++)
    for (int i=0;i<3;i++) {
      x->nman[i] += varNor();
      if (x->nman[i]<0.0) x->nman[i]=-x->nman[i];
    }
}

void River::PerturbBanks() {
  boost::mt19937 rnd;
  int LeftBank=0;
  int RightBank=0;
  int l1,l2,r1,r2;
  double xbl, xbr, xb;
  for (vector<Xsection>::iterator x=xs.begin();x!=xs.end();x++) {
    for (int i=0;i<x->nsta;i++) {
      if (x->x[i]==x->xbank[0])
	LeftBank=i;
      if (x->x[i]==x->xbank[1])
	RightBank=i;
    }
    if (LeftBank>0)
      l1=LeftBank-1;
    else 
      l1=0;
    l2=LeftBank+1;
    if (RightBank<(x->nsta-1))
      r2=RightBank+1;
    else
      r2=x->nsta-1;
    r1=RightBank-1;
    boost::uniform_real<double> udl(l1,l2), udr(r1,r2);
    boost::variate_generator<boost::mt19937&, boost::uniform_real<double> > varL(rnd,udl), varR(rnd,udr);
    xbl=varL();
    xbr=varR();
    if (xbl>xbr) {
      xb=xbr;
      xbr=xbl;
      xbl=xb;
    }
    x->x[LeftBank]=xbl;
    x->xbank[0]=x->x[LeftBank];
    x->x[RightBank]=xbr;
    x->xbank[1]=x->x[RightBank];
  }
}

vector<vector<double> > River::Banks() {
  vector<vector<double> > banks(xs.size(),vector<double>(2,0.0));
  for (size_t i=0;i<xs.size();i++) {
    banks[0][0]=xs[i].xbank[0];
    banks[0][1]=xs[i].xbank[1];
  }
  return banks;
}

void River::PerturbLateralInflows(double m, double s) {
  boost::mt19937 rnd;
  boost::normal_distribution<double> gd(m,s);
  boost::variate_generator<boost::mt19937&, boost::normal_distribution<double> > varNor(rnd,gd);
  for (size_t i=0;i<q.size();i++) {
    q[i]+=varNor();
    if (q[i]<=0.0) q[i]=-q[i];
  }
}

vector<double> River::Roughness() {
  vector<double> n(xs.size(),0.0);
  for (size_t i=0;i<xs.size();i++)
    n[i]=xs[i].nman[1];
  return n;
}

vector<double> River::Flow() {
  vector<double> Q(xs.size(),0.0);
  for (size_t i=0;i<xs.size();i++)
    Q[i]=flow[i];
  return Q;
}

void River::SetThalweg(vector<double>& zmin) {
  for (size_t i=0;i<xs.size();i++)
    xs[i].zmin=zmin[i];
}

vector<double> River::FlowArea(vector<double>& h) {
  vector<double> A(xs.size(),0.0);
  double P, K, T, alpha;
  for (size_t i=0;i<xs.size();i++)
    xs[i].CalcAPT(h[i],A[i],P,T,K,alpha);
  return A;
}

vector<double> River::TopWidth(vector<double>& h) {
  vector<double> T(xs.size(),0.0);
  double P, K, A, alpha;
  for (size_t i=0;i<xs.size();i++)
    xs[i].CalcAPT(h[i],A,P,T[i],K,alpha);
  return T;
}

