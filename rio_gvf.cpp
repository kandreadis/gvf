#include "river.hpp"

int main()
{
  string filename("RioGrande.g01");
  River rio(filename);
  double A, P, T, Kt, alpha;

  // remove empty cross-sections
  rio.RemoveSections();

  // correct bank location
  for (vector<Xsection>::iterator xs=rio.xs.begin();xs!=rio.xs.end();xs++)
    xs->CorrectBanks();

  // read flows and set boundary conditions
  double Q_bc=311.20;
  double h_bc=1135.63;
  rio.SetBoundary(Q_bc,h_bc);
  rio.ReadFlows("RioFlows0.txt");

  // change cross-section shape to rectangular
  // for (vector<Xsection>::iterator xs=rio.xs.begin();xs!=rio.xs.end();xs++)
  //   xs->Rectangular(xs->zmin);

  // solve for Gradually-Varied-Flow profile
  vector<double> h;
  h=rio.Solve();

  // for (int i=0;i<rio.xs.size();i++) {
  //   rio.xs[i].CalcAPT(h[i],A,P,T,Kt,alpha);
  //   cout << i << " " << T << " " << (h[i]-rio.xs[i].zmin) << endl;
  // }

  // write output
  rio.Write(h,"hr.dat");
}
